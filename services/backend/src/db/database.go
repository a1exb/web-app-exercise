package database

import (
    "database/sql"
    "fmt"
    "log"
    "os" // Don't forget to import the os package
    _ "github.com/lib/pq" // Import the PostgreSQL driver
)

// InitializeDatabase initializes the database connection.
func InitializeDatabase() (*sql.DB, error) {
    // Get the database connection parameters from environment variables
    dbHost := "postgres-service"
    dbPort := "5432"
    dbName := os.Getenv("DB_NAME")
    dbUser := os.Getenv("DB_USER")
    dbPassword := os.Getenv("DB_PASSWORD")

    // Construct the PostgreSQL connection string
    connStr := fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=disable",
        dbHost, dbPort, dbName, dbUser, dbPassword)

    // Open a connection to the PostgreSQL database
    db, err := sql.Open("postgres", connStr)
    if err != nil {
        return nil, err
    }

    return db, nil // Return the database connection
}

// CreateTableIfNotExists creates the required tables.
func CreateTableIfNotExists(db *sql.DB) {
    createTableQuery := `
        CREATE TABLE IF NOT EXISTS phone_book (
            id SERIAL PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            phone_number VARCHAR(20) NOT NULL
        )
    `

    _, err := db.Exec(createTableQuery)
    if err != nil {
        log.Fatal(err)
    }
}
