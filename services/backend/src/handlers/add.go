package handlers

import (
    "database/sql"
    "encoding/json"
    "log"
    "net/http"
)

func AddContactHandler(db *sql.DB) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
        log.Printf("Received request: %s %s", r.Method, r.URL.Path)
        // Handle preflight OPTIONS request
        if r.Method == http.MethodOptions {
            w.Header().Set("Access-Control-Allow-Origin", "*")
            w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
            w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
            w.WriteHeader(http.StatusOK)
            return
        }
        
        if r.Method == http.MethodPost {
            // Parse the JSON data from the request
            var contact struct {
                Name        string `json:"name"`
                PhoneNumber string `json:"phoneNumber"`
            }

            if err := json.NewDecoder(r.Body).Decode(&contact); err != nil {
                log.Printf("Error parsing request body: %v", err)
                log.Printf("Request body: %s", r.Body)
                http.Error(w, err.Error(), http.StatusBadRequest)
                return
            }
            // Insert the contact into the database
            insertQuery := "INSERT INTO phone_book (name, phone_number) VALUES ($1, $2)"
            _, err := db.Exec(insertQuery, contact.Name, contact.PhoneNumber)
            if err != nil {
                log.Printf("Error inserting data into the database: %v", err)
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
            }

            // Serialize the contacts to JSON and write the response
            addedContact := map[string]string{"name": contact.Name, "phone_number": contact.PhoneNumber}
            jsonBytes, err := json.Marshal(addedContact)
            if err != nil {
                log.Printf("Error marshaling JSON: %v", err)
                http.Error(w, "Internal Server Error", http.StatusInternalServerError)
                return
            }
            w.Header().Set("Access-Control-Allow-Origin", "*")
            w.Header().Set("Content-Type", "application/json")
            w.WriteHeader(http.StatusOK)
            w.Write(jsonBytes)
        } else {
            log.Printf("Method not allowed: %s %s", r.Method, r.URL.Path)
            http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
        }
    }
}
