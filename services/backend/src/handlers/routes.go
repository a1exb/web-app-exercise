package handlers

import (
    "net/http"
    "database/sql"
)

func SetupRoutes(db *sql.DB) {
    http.HandleFunc("/api/search-contacts", SearchContactsHandler(db))
    http.HandleFunc("/api/get-contacts", GetContactsHandler(db))
    http.HandleFunc("/api/add-contact", AddContactHandler(db))
    http.HandleFunc("/api/health", HealthHandler())
}
