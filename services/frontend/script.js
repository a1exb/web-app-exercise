// Function to clear the contacts table
function clearTable() {
    const contactsTable = document.getElementById("contactsTable");
    contactsTable.innerHTML = '<thead><tr><th>Name</th><th>Phone #</th></tr></thead><tbody></tbody>';
}

// Function to parse and place data in the contacts table
function populateTable(data) {
    clearTable(); // Clear the table before adding new data

    const contactsTable = document.getElementById("contactsTable");
    const tbody = contactsTable.querySelector("tbody");

    if (data) {
        data.forEach(contact => {
            const row = document.createElement("tr");
            const nameCell = document.createElement("td");
            const phoneCell = document.createElement("td");

            nameCell.textContent = contact.name;
            phoneCell.textContent = contact.phone_number;

            row.appendChild(nameCell);
            row.appendChild(phoneCell);
            tbody.appendChild(row);
        });
    } else {
        // Handle the case where data is null (no matches found)
        const noMatchesRow = document.createElement("tr");
        const noMatchesCell = document.createElement("td");
        noMatchesCell.setAttribute("colspan", 2);
        noMatchesCell.textContent = "No matching contacts found";
        noMatchesRow.appendChild(noMatchesCell);
        tbody.appendChild(noMatchesRow);
    }
}


async function loadBackendUrl() {
    try {
        const response = await fetch('backendUrl.json');
        if (!response.ok) {
            throw new Error('Failed to load backendUrl.json');
        }

        const data = await response.json();
        return data.backendUrl;
    } catch (error) {
        console.error('Error loading backend URL:', error);
        return null;
    }
}

async function addContact() {
    const name = document.getElementById("name").value;
    const phoneNumber = document.getElementById("phoneNumber").value;

    // Create a data object with the contact information
    const data = {
        name: name,
        phoneNumber: phoneNumber
    };

    const backendUrl = await loadBackendUrl();
    const backendServiceEp = 'http://' + backendUrl + '/api/add-contact';

    // Send a POST request to your backend API
    fetch(backendServiceEp, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then(response => response.json())
        .then(responseData => {
            // Handle the response from the backend
            // You can update the UI or display a success message here
            console.log('Contact added:', responseData);
        })
        .catch(error => {
            // Handle any errors that occur during the request
            console.error('Error adding contact:', error);
        });
}

// Function to handle the "Show All" button click
async function showAllContacts() {
    const backendUrl = await loadBackendUrl();
    const backendServiceEp = 'http://' + backendUrl + '/api/get-contacts';

    fetch(backendServiceEp)
        .then(response => response.json())
        .then(data => {
            populateTable(data);
        })
        .catch(error => {
            console.error('Error fetching contacts:', error);
        });
}

// Function to handle the "Search" button click
async function searchContacts() {
    const name = document.getElementById("name").value;
    const phoneNumber = document.getElementById("phoneNumber").value;

    const data = {
        name: name,
        phoneNumber: phoneNumber
    };

    const backendUrl = await loadBackendUrl();
    const backendServiceEp = 'http://' + backendUrl + '/api/search-contacts';

    fetch(backendServiceEp, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then(response => {
            if (response.status === 200) {
                // Check the response body content
                return response.text().then(responseText => {
                    if (responseText === 'null') {
                        // Response body is "null"; no matches found
                        return null;
                    } else {
                        // Response body has data; proceed to parse JSON
                        return JSON.parse(responseText);
                    }
                });
            } else {
                // Handle non-successful response (e.g., status code 4xx or 5xx)
                console.error('Request failed with status:', response.status);
                throw new Error('Request failed');
            }
        })
        .then(data => {
            populateTable(data);
        })
        .catch(error => {
            console.error('Error searching contacts:', error);
        });
}

// Attach the functions to the buttons
document.getElementById("addButton").addEventListener("click", addContact);
document.getElementById("showAllButton").addEventListener("click", showAllContacts);
document.getElementById("searchButton").addEventListener("click", searchContacts);
