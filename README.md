# web-app-exercise

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/send-notification-web-page/web-app-exercise.git
git branch -M main
git push -uf origin main
```

## Demo setup procedure

### Deploy EKS cluster (optional)
1. Install terraform version 1.5.7 and the latest terragrunt version
1. Set **AWS_ACCESS_KEY_ID** and **AWS_SECRET_ACCESS_KEY** values. Make sure the IAM user which is used to crete the EKS cluster has all the proper permissions.
1. Set AWS account id in */ifrastructure/dev-account/account.hcl*
1. Navigate to /ifrastructure/dev-account/us-east-1/eks/cluster and execute terragurnt apply command
1. To configure kubectl use the following command: 
    ```
    aws eks update-kubeconfig --region aws-region --name cluster-name
    ```

### Build backend image (optional - alternately use alexbespalov0/backend pre build image)
1. Use any linux amd64 machine of your choice. Make sure Docker, golang 1.18 and Git are installed. Alternately other image building tools could be used. Ex.: Kaniko
1. Clone *web-app-excercise* repository to the user directory
1. Login to your dockerhub repository
1. Navigate to *web-app-excercise/services/backend* directory and run the following command:
    ```
    sudo CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o bin/backend-service src/main.go
    ```
1. Set **DOCKER_REPOSITORY** to an appropriate value and run the following command:
    ```
    DOCKER_REPOSITORY=<your-repo-name>; TAG=0.0.1; SERVICENAME=backend; sudo docker build -t $SERVICENAME:$TAG . && sudo docker tag $SERVICENAME:$TAG $DOCKER_REPOSITORY/$SERVICENAME:$TAG && sudo docker push $DOCKER_REPOSITORY/$SERVICENAME:$TAG
    ``` 

### Deploying postgres db and backend service
1. Clone the *web-app-excercise* repository to the machine which has kubectl configured to access the K8S cluster to which the application will be deployed.
1. **NOTE** if you're building your own backend image don't forget to modify the image name in the *deployment.yaml*
1. From repository root directory execute the following commands: 
    ```
    kubectl apply -f ./manifests/postgress-db
    kubectl apply -f ./manifests/backend
    ```

### Build frontend image (optional - alternately use alexbespalov0/frontend pre build imag)
1. Use any linux amd64 machine of your choice. Make sure Docker, and Git are installed. Alternately other image building tools could be used. Ex.: Kaniko
1. Clone *web-app-excercise* repository to the user directory
1. **IMPORTANT** run `kubectl get svc` command and note the external IP of the backend service. Modify /services/frontend/backendUrl.json file accordingly
1. Login to your dockerhub repository
1. Set **DOCKER_REPOSITORY** to an appropriate value and run the following command: 
    ```
    DOCKER_REPOSITORY=alexbespalov0; TAG=0.0.1; SERVICENAME=frontend; sudo docker build -t $SERVICENAME:$TAG . && sudo docker tag $SERVICENAME:$TAG $DOCKER_REPOSITORY/$SERVICENAME:$TAG && sudo docker push $DOCKER_REPOSITORY/$SERVICENAME:$TAG
    ```

### Deploying frontend service
1. Clone the *web-app-excercise* repository to the machine which has kubectl configured to access the K8S cluster to which the application will be deployed.
1. **NOTE** if you're building your own backend image don't forget to modify the image name in the *deployment.yaml*
1. From repository root directory execute the following commands: 
    ```
    kubectl apply -f ./manifests/frontend
    ```

**NOTE**
If you chose not to build your own frontend image, you could replace the content of the backendUrl.json directly in the container using this commands:
```
kubectl exec -it <frontend-pod-name> -- sh -c "vi /usr/local/apache2/htdocs/backendUrl.json"
```

### Accessing the app
Execute the following command:
```
kubectl get svc
```
Note the external IP of the frontend service. The app could be accessed at this URL in any browser.