locals {
  module_source_url = "${get_repo_root()}//modules/eks"

  tags = {
    org-name = include.root.inputs.org_name
  }
}

include "root" {
  path   = find_in_parent_folders("common_config.hcl")
  expose = true
}

terraform {
  source = local.module_source_url
}

inputs = {
  region                 = include.root.inputs.aws_region
  org_name               = include.root.inputs.org_name
  tags                   = local.tags
  vpc_cidr               = "10.0.0.0/16"
  public_subnet_cidrs    = ["10.0.0.0/24", "10.0.1.0/24"]
  nodegroup_desired_size = 2
  nodegroup_max_size     = 2
  nodegroup_min_size     = 2
}
